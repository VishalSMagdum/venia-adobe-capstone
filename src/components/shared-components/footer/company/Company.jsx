import React from "react";
import "./Company.scss";

const Company = () => {
    return (<>
        <main className="main-company">
            <p className="name-company">
                © Company Name <br /> Address Ave, City Name, State ZIP
            </p>
        </main>
    </>)
}

export default Company;