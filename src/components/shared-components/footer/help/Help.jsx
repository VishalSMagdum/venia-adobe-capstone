import React from "react";
import "./Help.scss";

const Help = () => {
    return (<>
        <main className="main-help">
            <p className="title-help">Help</p>
            <p className="live-chat-help">Live Chat</p>
            <p className="contact-us-help">Contact us</p>
            <p className="order-status-help">Order Status</p>
            <p className="return-help">Returns</p>
        </main>
    </>)
}

export default Help;