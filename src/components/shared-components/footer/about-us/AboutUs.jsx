import React from "react";
import "./AboutUs.scss";

const AboutUs = () => {
    return (<>
        <main className="main-about-us">
            <p className="title-about-us">About Us</p>
            <p className="our-story-about-us">Our Story</p>
            <p className="email-sign-up-about-us">Email Sign Up</p>
            <p className="give-back-about-us">Give Back</p>
        </main>
    </>)
}

export default AboutUs;