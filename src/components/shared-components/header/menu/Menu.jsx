import React from "react";

const Menu = () => {
    return (<>
        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 20 14">
            <g id="menu" transform="translate(-2 -5)">
                <line id="Line_510" dataname="Line 510" x2="18" transform="translate(3 12)" fill="none" stroke="#FFFFFF" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" />
                <line id="Line_511" dataname="Line 511" x2="18" transform="translate(3 6)" fill="none" stroke="#FFFFFF" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" />
                <line id="Line_512" dataname="Line 512" x2="18" transform="translate(3 18)" fill="none" stroke="#FFFFFF" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" />
            </g>
        </svg>

    </>)
}

export default Menu;