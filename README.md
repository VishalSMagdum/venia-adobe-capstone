# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Adobe React Assignment

### `Design`

Style guide: https://xd.adobe.com/view/20be0a8d-cdec-4ab5-8589-e52ed5daaf75-75ed/

Mobile Flow: https://xd.adobe.com/view/79845552-030d-4e09-b50e-57a07f669f0b-2cd7/

Desktop Flow: https://xd.adobe.com/view/b9e906e3-450d-41d3-a83b-8e5083dccd4c-ecba/


### `API details`

https://fakestoreapi.com/docs

### `Requirements`

Create below flow as per the design using React framework,

1.	Home

2.	Product Listing

3.	Product Details

4.	Cart

5.	Checkout

6.	Order Summary

### `Technology stack`

- HTML5

- CSS/SCSS

- React JS

- Redux for state management


## Solution Approch

- nodejs --version 14.7.0 

- npm --version 6.14.7 

- react --version 18.1.0 

## plugins

- axios --version 0.27.2 

- @reduxjs/toolkit --version 1.8.2

- @mui/material --version 5.8.3 

- sass --version 1.52.3 


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!


## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Light House Report

### Desktop

![Light house Image](./src/assets/report/Report_2.png)

### Mobile

![Light house Image](./src/assets/report/Report_3.png)



## Git Labs Repository

https://gitlab.com/VishalSMagdum/venia-adobe-capstone.git

## Heroku Deployment steps 

- $ heroku login 

- $ git pull 

- $ git add . 

- $ git commit -m "initial commit" 

- $ git push heroku master 

- $ heroku open

## Heroku Deployment Site URL

https://venia-adobe-capstone.herokuapp.com/
